// Package
package project.reciever;

// Import 
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import project.utils.Packet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import project.utils.PacketSenderThread;
import project.utils.PacketType;

/**
 * This is the main class for Recieving files from the sender
 * @author Aziel Shaw
 */
public class Reciever {

    // Local class variables
    private final int PORT = 9898; // Port number
    private Packet initial_packet; // Store initial packet
    private ArrayList<Packet> packetsList; // Store all the packets
    private boolean[] addingArray; // Make sure we only store unique packets
    private DatagramSocket rSocket; // Reciver socket
    private DatagramSocket sSocket; // Sender socket
    private final int TEMP_FILE_SIZE = 10048576; // Initial file size, good practice states it must be bigger than most files it'll send.
    private MessageDigest digest; // To perform checksums
    private String senderIP; // IP of the sender to send acknowledgements

    /**
     * Constructor for the Reciever
     */
    public Reciever() {
        try {
            rSocket = new DatagramSocket(PORT);
            sSocket = new DatagramSocket();
            digest = MessageDigest.getInstance("SHA-256");
            packetsList = new ArrayList<>();
        } catch (SocketException ex) {
            Logger.getLogger(Reciever.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException nsae) {
            Logger.getLogger(Reciever.class.getName()).log(Level.SEVERE, null, nsae);
        }
    }

    /**
     * Main method for the Reciever
     * @param args commandline arguments
     * @throws IOException throws exception
     */
    public void main(String[] args) throws IOException {
        // Get initial Packet
        initial_packet = recievePacket();
        // Get total number of packets from the first packet
        int numPackets = initial_packet.getTotalNumPacket();
        int totalpktsize = initial_packet.getTotalPktSize();
        byte[] fileCheckSum = initial_packet.getData();
        String fileName = initial_packet.getFileName();
        addingArray = new boolean[numPackets];
        for(boolean b : addingArray) b = false;
        // Print contents of the initial packet
        System.out.println("SenderIP: " + senderIP);
        System.out.println(initial_packet);
        System.out.println("Total num packets = " + numPackets);
        // Get the rest of the packets while we haven't gotten all the packets
        while (!transferComplete(numPackets)) {
            // Recieve packet
            Packet mostRecentPacket = recievePacket();
            // Get digest checksum
            byte[] newestDigest = digest.digest(mostRecentPacket.getData());
            // Compare checksums
            if (checkCheckSums(newestDigest, mostRecentPacket.getCheckSum())) {
                // If we haven't already heard from the packet at this index, add it.
                if(!addingArray[mostRecentPacket.getSequenceNum() - 1]) {
                    packetsList.add(mostRecentPacket);
                    addingArray[mostRecentPacket.getSequenceNum() - 1] = true;
                }
                // Print packet status
                System.out.print("Recieved Packet: "
                        + mostRecentPacket.getSequenceNum()
                        + "/"
                        + mostRecentPacket.getTotalNumPacket());
                System.out.println(String.format(", %.2f", (((float) packetsList.size() / numPackets) * 100)) + "% done.");
                System.out.println("Checkum Equal");
                // Send Ack
                Packet p = new Packet(PacketType.ACKNOWLEDGEMENT, new byte[0], mostRecentPacket.getSequenceNum(), mostRecentPacket.getTotalNumPacket());
                new PacketSenderThread(this, sSocket, false, p, senderIP, PORT).start();
                System.out.println("Sending Acknowledge");
            } else {
                System.out.println("Checkum NOT Equal");
                // Send Rexmit
                Packet p = new Packet(PacketType.RETRANSMIT, new byte[0], mostRecentPacket.getSequenceNum(), mostRecentPacket.getTotalNumPacket());
                new PacketSenderThread(this, sSocket, false, p, senderIP, PORT).start();
                System.out.println("Sending Retransmit");
            }
        }
        System.out.println("");
        // Sort array
        System.out.println("Reordering packets");
        Collections.sort(packetsList);
        byte[] newfile = new byte[totalpktsize];
        // Combine new bytep[]'s into file
        int upto = 0;
        Iterator<Packet> itr = packetsList.iterator();
        while (itr.hasNext()) {
            Packet p = itr.next();
            int counter = 0;
            for (int i = upto; i < (upto + p.getData().length); i++) {
                try {
                    newfile[i] = p.getData()[counter++];
                } catch(ArrayIndexOutOfBoundsException e) {
                    System.out.println("ArrayIndexOutOfBounds");
                    System.out.println("i = " + i);
                }
            }
            upto += p.getData().length;
        }
        // Store file
        FileOutputStream fileOuputStream = null;
        try {
            String[] path = fileName.split("\\/");
            fileOuputStream = new FileOutputStream("target/" + path[path.length - 1]);
            fileOuputStream.write(newfile);
            fileOuputStream.close();
        } catch (IOException ioe) {
            System.out.println("Error Saving file");
            System.out.println(ioe.getLocalizedMessage());
            ioe.printStackTrace();
        }
        // Final Check & Print Success/Failure
        if (checkCheckSums(fileCheckSum, digest.digest(newfile))) {
            System.out.println("File Transfer Complete");
        } else {
            System.out.println("File Transfer Failed");
        }

    }

    // Write Method to handle recieving Packets
    private Packet recievePacket() {
        byte[] buffer = new byte[TEMP_FILE_SIZE];
        DatagramPacket packet = new DatagramPacket(buffer, TEMP_FILE_SIZE);
        ByteArrayInputStream byteInStream = null;
        ObjectInputStream inStream = null;
        Packet packetObj = null;
        // Set up socket
        try {
            rSocket.receive(packet);
            buffer = packet.getData();
        } catch (IOException iOException) {
            System.err.println("Something went wrong:");
            System.err.println("MessageRecieverThread run method");
            System.err.println(iOException.getMessage());
            System.err.println(iOException.getLocalizedMessage());
            Logger.getLogger(Reciever.class.getName()).log(Level.SEVERE, null, iOException);
        }
        // Get the IP address of the Sender
        if (senderIP == null) {
            senderIP = packet.getAddress().getHostAddress();
        }
        // Get Data from packet
        try {
            byteInStream = new ByteArrayInputStream(buffer);
            inStream = new ObjectInputStream(byteInStream);
            packetObj = (Packet) inStream.readObject();
        } catch (ClassNotFoundException cnfe) {
            Logger.getLogger(Reciever.class.getName()).log(Level.SEVERE, null, cnfe);
        } catch (IOException ioe) {
            Logger.getLogger(Reciever.class.getName()).log(Level.SEVERE, null, ioe);
        }
        return packetObj;
    }

    /**
     * This method returns if the transfer of packets is complete
     * @param numPackets The total number of packets to be transferred
     * @return If done sending
     */
    private boolean transferComplete(int numPackets) {
        return packetsList.size() == numPackets;
    }

    /**
     * This method takes in two byte arrays of checksums and checks if they 
     * are the same
     * @param b1 The first checksum
     * @param b2 The second checksum
     * @return If the checksums are the same
     */
    private boolean checkCheckSums(byte[] b1, byte[] b2) {
        // If the checksums are different lengths then they are not the same.
        if (b1.length != b2.length) {
            return false;
        }
        boolean success = true;
        // Check if each byte is equal
        for (int i = 0; i < b1.length && success; i++) {
            if (b1[i] != b2[i]) {
                success = false;
            }
        }
        // Return success
        return success;
    }

}
