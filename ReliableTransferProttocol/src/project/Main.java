// Package
package project;

// Import
import java.io.IOException;
import project.sender.Sender;
import project.reciever.Reciever;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 * Launcher for the Reliable Data Transfer Protocol
 * @author Aziel Shaw
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException, NoSuchAlgorithmException, IOException, InterruptedException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Sender or Reciever?");
        String type = scan.nextLine();
        // Launch Sender
        if(type.equalsIgnoreCase("Sender") || type.equalsIgnoreCase("s")) {
            System.out.println("Starting Sender");
            Sender s = new Sender();
            s.main(args);
        // Launch Reciever
        } else if(type.equalsIgnoreCase("Reciever") || type.equalsIgnoreCase("r")) {
            System.out.println("Starting Reciever, this IP is: " + InetAddress.getLocalHost().getHostAddress());
            Reciever r = new Reciever();
            r.main(args);
        // Quit
        } else {
            System.out.println("Input not recognized, please try again.");
        }
    }
    
}
