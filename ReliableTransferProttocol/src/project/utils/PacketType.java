// Packets
package project.utils;

/**
 * Enumeration for the different types of packets that can be sent
 * @author Aziel Shaw
 */
public enum PacketType {
    // Enumeration for the initial packet
    INITIAL_PACKET("Initial Packet"),
    // Enumeration for the sending of data
    DATASEND("Sending Data"), 
    // Enumeration for the acknowledgement of data
    ACKNOWLEDGEMENT("Acknowledgement"), 
    // Enumeration for the retransmission of data
    RETRANSMIT("Retransmitted");
    
    // Local String instance
    private final String string;
    
    /**
     * 
     * @param s String of the object
     */
    PacketType(String s) {
        this.string = s;
    }

    /**
     * The string representation of this enumerated type
     * @return the string representation
     */
    private String string() { 
        return this.string; 
    }
}
