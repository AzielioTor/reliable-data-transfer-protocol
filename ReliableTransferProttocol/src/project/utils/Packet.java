// Package
package project.utils;

// Imports
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is a container for the packet information that is passed between clients
 * @author Aziel Shaw
 */
public class Packet implements Comparable<Packet>, Serializable {

    // Local class variables
    private PacketType packettype;
    private byte[] checksum;
    private byte[] data;
    private int sequence;
    private int totalpacketnumbers;
    private int totalpacketsize;
    private String fileName;
    
    /**
     * This constructor is for the normal packets that are sent between hosts 
     * @param pt PacketType
     * @param d Data array to send
     * @param seq Packet Sequence Number
     * @param totalpktnums Total number of packets to send
     */
    public Packet(PacketType pt, byte[] d, int seq, int totalpktnums) {
        this.packettype = pt;
        this.data = d;
        this.sequence = seq;
        this.totalpacketnumbers = totalpktnums;
        this.totalpacketsize = -1;
        try {
            // Get Checksum of data
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            this.checksum = digest.digest(this.data);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Packet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    /**
     * This constructor is for an initial packet to be sent to the other host.
     * @param pt The type of packet, for this constructor it's generally an INITIALPACKET
     * @param d Data to send, generally for this constructor it is the checksum of the total file.
     * @param seq the sequence number
     * @param pktnum The packetnumber 
     * @param totalPktSize The total size of the file to be transfered
     * @param fileName The name of the file
     */
    public Packet(PacketType pt, byte[] d, int seq, int pktnum, int totalPktSize, String fileName) {
        this(pt, d, seq, pktnum);
        this.totalpacketsize = totalPktSize;
        this.fileName = fileName;
    }
    
    /**
     * Returns the type of packet this is
     * @return the type of packet this is
     */
    public PacketType getPacketType() {
        return this.packettype;
    }
    
    /**
     * Returns the sequence number of the packet
     * @return the sequence number of the packet
     */
    public int getSequenceNum() {
        return this.sequence;
    }
    
    /**
     * Returns the total number of packets
     * @return the total number of packets
     */
    public int getTotalNumPacket() {
        return this.totalpacketnumbers;
    }
    
    /**
     * Returns the data of this packet
     * @return the data of this packet
     */
    public byte[] getData() {
        return this.data;
    }
    
    /**
     * Returns the checksum of the data of this packet
     * @return the checksum of the data of this packet
     */
    public byte[] getCheckSum() {
        return this.checksum;
    }
    
    /**
     * Returns the total size of the packet
     * @return the total size of the packet
     */
    public int getTotalPktSize() {
        return this.totalpacketsize;
    }
    
    /**
     * Returns the file name of the file.
     * @return the file name of the file.
     */
    public String getFileName() {
        return this.fileName;
    }
    
    /**
     * Compares two packets and returns an ordering of the two.
     * Comparison is based on the sequence number of the packets
     * @param o The other packet to be compared
     * @return Which packet is higher/lower, or 0 if they are equal.
     */
    @Override
    public int compareTo(Packet o) {
        if(this.getSequenceNum() < o.getSequenceNum()) {
            return -1;
        } else if(this.getSequenceNum() > o.getSequenceNum()) {
            return 1;
        } else {
            return 0;
        }
    }
    
    /**
     * Returns the string representation of the packet
     * @return the string representation of the packet
     */
    @Override
    public String toString() {
        StringBuffer toReturn = new StringBuffer();
        toReturn.append("PacketType = " + this.packettype + "\n");
        toReturn.append("Packet: " + this.sequence + "/" + this.totalpacketnumbers + "\n");
        return toReturn.toString();
    }
    
}
