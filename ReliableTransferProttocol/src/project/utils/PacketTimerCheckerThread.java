// Package
package project.utils;

// Imports
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import project.sender.Sender;

/**
 * A thread to check the timeout of a particular packet
 * @author Aziel Shaw
 */
public class PacketTimerCheckerThread extends Thread {
    
    // Local class threads
    private final int TIMEOUT = 800;
    private final Sender owner;
    private boolean keeprunning;
    private Packet packet;
    
    /**
     * Constructor for the PacketTimerCheckerThread
     * @param s The sender who 'owns' this thread
     * @param p The packet to check the timeout for
     */
    public PacketTimerCheckerThread(Sender s, Packet p) {
        this.owner = s;
        keeprunning = true;
        packet = p;
    }
    
    /**
     * Main loop of the thread
     */
    @Override
    public void run() {
        do {
            // Sleep thread
            try {
                Thread.sleep(TIMEOUT);
            } catch (InterruptedException ex) {
                Logger.getLogger(PacketTimerCheckerThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            // Check for timeout
            HashMap<Integer, String> table = owner.getResponseTable();
            String value = table.get(packet.getSequenceNum());
            if(value.equalsIgnoreCase("NonAck")) {
                new PacketSenderThread(owner, 
                        owner.getSenderSocket(), 
                        false, 
                        owner.getPacketTable().get(packet.getSequenceNum()),
                        owner.getIP(), 
                        owner.getPORT())
                        .start();
                new PacketTimerCheckerThread(owner, packet).start();
                owner.packetFailure();
            } else if(value.equalsIgnoreCase("Retransmit")) {
                keeprunning = false;
            } else {
                keeprunning = false;
                owner.getPacketTable().remove(packet.getSequenceNum());
            }
            keeprunning = false;
        } while(keeprunning);
    }
    
    /**
     * Kills the thread
     */
    private void killThread() {
        this.keeprunning = false;
    }
    
}
