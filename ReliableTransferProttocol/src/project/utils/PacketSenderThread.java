// Package
package project.utils;

// Importts
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import project.reciever.Reciever;
import project.sender.Sender;
import project.utils.Packet;

/**
 * This class handles the sending of packets for Senders/Receivers
 * @author Aziel Shaw
 */
public class PacketSenderThread extends Thread {
    
    // Local class variables
    private final int PORT;
    private DatagramSocket senderSocket;
    private boolean keeprunning;
    private Packet packet;
    private Sender sOwner;
    private Reciever rOwner;
    private String ip;
    
    /**
     * Constructor for the packetsender for the sender
     * @param s The Sender owner
     * @param ss The Datagram socket to send over
     * @param running Whether to continue runninght the thread
     * @param toSend Packet to send
     * @param ip IP to send to
     * @param port Port to send to
     */
    public PacketSenderThread(Sender s, DatagramSocket ss, boolean running, 
            Packet toSend, String ip, int port) {
        this.sOwner = s;
        this.senderSocket = ss;
        this.keeprunning = running;
        this.packet = toSend;
        this.ip = ip;
        this.PORT = port;
    }
    
    /**
     * Constructor for the packetsender for the reciever
     * @param r Receiver owner
     * @param ss The Datagram socket to send over
     * @param running Whether to continue running the thread
     * @param toSend Packet to send
     * @param ip IP to send to
     * @param port Port to send to
     */
    public PacketSenderThread(Reciever r, DatagramSocket ss, boolean running, 
            Packet toSend, String ip, int port) {
        this.rOwner = r;
        this.senderSocket = ss;
        this.keeprunning = running;
        this.packet = toSend;
        this.ip = ip;
        this.PORT = port;
    }
    
    /**
     * Main method of the thread to run and send packets
     */
    @Override
    public void run() {
        // Create buffer
        byte[] buffer = packetToByteArray(this.packet);
        // Get IP Address as InetAddress
        InetAddress address = null;
        try {
            address = InetAddress.getByName(ip);
        } catch (UnknownHostException ex) {
            Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Create DatagramPacket to set
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, this.PORT);
        try {
            // Send packet
            senderSocket.send(packet);
        } catch (IOException ex) {
            Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * This method takes a packet and turns it into a byte array
     * @param p Packet to be turned into byte array
     * @return The byte array of the packet
     */
    public byte[] packetToByteArray(Packet p) {
        // Buffer
        byte[] buffer = null;
        // Needed Streams
        ByteArrayOutputStream byteOutStream = null;
        ObjectOutputStream outStream = null;
        try {
            byteOutStream = new ByteArrayOutputStream();
            outStream = new ObjectOutputStream(byteOutStream);
            // Write copied table to byte[]
            outStream.writeObject(p);
            outStream.flush();
            buffer = byteOutStream.toByteArray();
        } catch(IOException ioe) {
            System.err.println(ioe.toString());
            ioe.printStackTrace();
        } finally {
            // Close streams
            try {
                if(outStream != null) {
                    outStream.close();
                }
                if(byteOutStream != null) {
                    byteOutStream.close();
                }
            } catch(IOException ioe) {
                System.err.println(ioe.toString());
                ioe.printStackTrace();
            }
        }
        // Return byte array buffer
        return buffer;
    }
    
}
