// Package
package project.sender;

// Import
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import project.reciever.Reciever;
import project.utils.Packet;
import project.utils.PacketSenderThread;
import project.utils.PacketTimerCheckerThread;
import project.utils.PacketType;

/**
 * Thread which handles the recieving of packets
 * @author Aziel Shaw
 */
public class PacketRecieverThread extends Thread {
    
    // Local class variables
    private DatagramSocket socket;
    private Sender owner;
    private final int TEMP_FILE_SIZE = 1048576;
    private final int sleepTime = 0;
    public static final String REXMIT = "Retransmit";
    public static final String ACK = "Acknowledge";
    private boolean running;
            
    /**
     * Constructor for the packet reciever thread
     * @param s The sender who owns this tread
     * @param PORT The port number
     * @throws SocketException Socket exception
     */
    public PacketRecieverThread(Sender s, int PORT) throws SocketException {
        this.owner = s;
        this.socket = new DatagramSocket(PORT);
        running = true;
    }
    
    /**
     * Kills the thread
     */
    public void killThread() {
        this.running = false;
    }
    
    /**
     * Main run of the thread
     */
    @Override
    public void run() {
        // Get null packet
        Packet p =  null;
        // While continue running
        while(running) {
            // Get packet
            p = recievePacket();
            // If the packet is a retransmit packet, resend the packet and kill this thread
            if(p.getPacketType() == PacketType.RETRANSMIT) {
                owner.addToResponseTable(p.getSequenceNum(), REXMIT);
                System.out.println("Packet retransmitted seq num: " + p.getSequenceNum());
                new PacketSenderThread(owner, 
                        owner.getSenderSocket(), 
                        false, 
                        owner.getPacketTable().get(p.getSequenceNum()),
                        owner.getIP(), 
                        owner.getPORT())
                        .start();
                new PacketTimerCheckerThread(owner, p).start();
            // If the packet is an acknowledgement, add to table and kill thread
            } else if(p.getPacketType() == PacketType.ACKNOWLEDGEMENT) {
                owner.addToResponseTable(p.getSequenceNum(), ACK);
                System.out.println("Packet acknowledged seq num: " + p.getSequenceNum());
            // The only packets should be sent are REXMIT and ACK
            } else {
                System.out.println("PROBLEM!!");
            }
        }
    }
    
    /**
     * This method handles the recieving of packets
     * @return The packet that has arrived at the client
     */
    private Packet recievePacket() {
        // Create temp buffer for file
        byte[] buffer = new byte[TEMP_FILE_SIZE];
        // Make Datagrampacket 
        DatagramPacket packet = new DatagramPacket(buffer, TEMP_FILE_SIZE);
        // Create streams
        ByteArrayInputStream byteInStream = null;
        ObjectInputStream inStream = null;
        Packet packetObj = null;
        // Set up socket
        try {
            // Get packet
            socket.receive(packet);
            // Get packets data
            buffer = packet.getData();
        } catch (IOException iOException) {
            System.err.println("Something went wrong:");
            System.err.println("MessageRecieverThread run method");
            System.err.println(iOException.getMessage());
            System.err.println(iOException.getLocalizedMessage());
            Logger.getLogger(Reciever.class.getName()).log(Level.SEVERE, null, iOException);
        }
        // Get Data from packet
        try {
            byteInStream = new ByteArrayInputStream(buffer);
            inStream = new ObjectInputStream(byteInStream);
            packetObj = (Packet) inStream.readObject();
        } catch (ClassNotFoundException cnfe) {
            Logger.getLogger(Reciever.class.getName()).log(Level.SEVERE, null, cnfe);
        } catch (IOException ioe) {
            Logger.getLogger(Reciever.class.getName()).log(Level.SEVERE, null, ioe);
        }
        // Return packet
        return packetObj;
    }
}
