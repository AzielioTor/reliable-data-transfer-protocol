// Package
package project.sender;

// Imports
import project.utils.PacketSenderThread;
import project.utils.Packet;
import project.utils.PacketType;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import project.utils.PacketTimerCheckerThread;

/**
 * This class handles the sending of a file to the reciever
 * @author Aziel Shaw
 */
public class Sender {

    // Local class variables
    private final int PORT = 9898;
    private final int MTU = 256; // In bytes ("congestion window") // NOTE make sure I don't go outside bound of UDP packet
    private final int INITIAL_SLEEP = 800;
    private final int TIMEOUT = 1000;
    private byte[] data;
    private DatagramSocket senderSocket;
    private String IP;
    private int ssThresh = 512;
    private int cwnd = 1;
    private int numPackets;
    private HashMap<Integer, String> responseTable;
    private HashMap<Integer, Packet> packetTable;
    private boolean failure = false;

    /**
     * Constructor for the Sender class
     * @throws SocketException 
     */
    public Sender() throws SocketException {
        responseTable = new HashMap<Integer, String>();
        packetTable = new HashMap<Integer, Packet>();
    }

    /**
     * Gets the response table
     * @return The response table
     */
    public HashMap<Integer, String> getResponseTable() {
        return this.responseTable;
    }
    
    /**
     * This method is for other classes to indicate to the Sender that there was 
     * a packet failure somewhere in transmission
     */
    public void packetFailure() {
        this.failure = true;
    }

    /**
     * Adds a value to the response table based on a key
     * @param key The key to add
     * @param value The value to add
     */
    public void addToResponseTable(Integer key, String value) {
        getResponseTable().put(key, value);
    }

    /**
     * Removes a key/value pair from the reponse table
     * @param key The key to remove
     */
    public void removeFromResponseTable(Integer key) {
        getResponseTable().remove(key);
    }

    /**
     * Gets the packettable which stores all the confirmed not successfully sent packets
     * @return The packettable
     */
    public HashMap<Integer, Packet> getPacketTable() {
        return this.packetTable;
    }

    /**
     * Adds a packet to the packet table 
     * @param key Key
     * @param value Value
     */
    public void addToPacketTable(Integer key, Packet value) {
        getPacketTable().put(key, value);
    }

    /** 
     * Removes a packet from the packet table based on key
     * @param key The key to remove
     */
    public void removeFromPacketTable(Integer key) {
        getPacketTable().remove(key);
    }

    /**
     * Returns the Datagramsocket we are sending over
     * @return the Datagramsocket we are sending over
     */
    public DatagramSocket getSenderSocket() {
        return this.senderSocket; 
    }

    /**
     * Returns the IP to send
     * @return the IP to send
     */
    public String getIP() {
        return this.IP;
    }

    /**
     * Returns the port to send
     * @return the port to send
     */
    public int getPORT() {
        return this.PORT;
    }

    /**
     * Main method for the Sender
     * @param args Commandline arguments
     * @throws NoSuchAlgorithmException Throws NoSuchAlgorithmException
     * @throws InterruptedException Throws InterruptedException
     * @throws SocketException Throws SocketException
     */
    public void main(String[] args) throws NoSuchAlgorithmException, InterruptedException, SocketException {
        // Get IP address
        Scanner scan = new Scanner(System.in);
        System.out.println("Please type the IP address you want to send too");
        IP = scan.nextLine();
        while (!isValidIP(IP)) {
            System.out.println("Please type a valid IP address");
            IP = scan.nextLine();
        }
        // Get file
        System.out.println("Please type a file to transfer (reccomended\"files/textbook.pdf\")");
        String fileName = scan.nextLine();
        File file = new File(fileName);
        // If file doesn't exist
        while (!file.canRead()) {
            System.out.println("Can't read file, please type another file.");
            fileName = scan.nextLine();
            file = new File(fileName);
        }
        // file into byte array
        data = new byte[(int) file.length()];
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try {
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            bis.read(data, 0, data.length);
        } catch (FileNotFoundException fnfe) {
            System.out.println("Something went wrong reading the file..");
            System.out.println("Exiting...");
        } catch (IOException ioe) {
            System.out.println("Something went wrong reading the file..");
            System.out.println("Exiting...");
        }
        // split file into packets
        numPackets = (int) Math.ceil((float) data.length / MTU);
        System.out.println("Number of packets: " + numPackets + "  Data length: " + data.length);
        Packet[] packets = new Packet[numPackets];
        int counter = 0;
        for (int i = 0; i < data.length; i = i + MTU) {
            int upperbound = i + MTU;
            if (upperbound >= data.length) {
                upperbound = data.length;
            }
            byte[] packetData = Arrays.copyOfRange(data, i, upperbound);
            // Create packets
            packets[counter] = new Packet(PacketType.DATASEND, packetData, (counter + 1), numPackets);
            counter++;
        }
        // Start packet sending CREATE Receiver thread
        PacketRecieverThread prt = new PacketRecieverThread(this, PORT);
        prt.start();
        System.out.println("Packets created");
        // Set up everything needed to send packets
        try {
            senderSocket = new DatagramSocket();
        } catch (SocketException se) {
            System.out.println(se.getMessage());
            se.printStackTrace();
        }
        // Send out initial packet
        byte[] firstCheckSum = MessageDigest.getInstance("SHA-256").digest(data);
        Packet initialp = new Packet(PacketType.INITIAL_PACKET, firstCheckSum, 0, numPackets, data.length, fileName);
        new PacketSenderThread(this, senderSocket, false, initialp, IP, PORT).start();
        // Sleep Thread before starting packet send
        try {
            Thread.sleep(INITIAL_SLEEP);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
        }
        // send packets
        sendPackets(packets, numPackets);
        prt.killThread();
        // print success
        Thread.sleep(500);
        System.out.println("All packets sent successfully");
        System.exit(0);
    } // End main

    /**
     * Takes a string and makes sure it is a valid IP address
     * @param ip IP address
     * @return If IP Address is valid
     */
    private boolean isValidIP(String ip) {
        boolean valid = true;
        String[] octets = ip.split("\\.");
        if (octets.length != 4) {
            valid = false;
        }
        Integer i;
        for (String s : octets) {
            i = Integer.parseInt(s);
            if (i < 0 || i > 255) {
                valid = false;
            }
        }
        return valid;
    }

    /**
     * This method handles the sending of packets for the sender
     * @param packets The packets to send
     * @param numPackets The total number of packets
     * @throws InterruptedException Throws InterruptedException
     */
    public void sendPackets(Packet[] packets, int numPackets) throws InterruptedException {
        for(int i = 1; i <= numPackets;) {
            // Send packets in Congestion Window
            ArrayList<PacketTimerCheckerThread> timerThreads = new ArrayList<>();
            for (int j = i; j <= Math.min(i + cwnd, numPackets); j++) {
                // Get Packet
                Packet p = packets[j - 1];
                new PacketSenderThread(this, senderSocket, false, p, IP, PORT).start();
                // Insert information into tables
                responseTable.put(p.getSequenceNum(), "NonAck");
                packetTable.put(p.getSequenceNum(), p);
                // Print status
                System.out.print("Sent Packet, " + p.getSequenceNum() + "/" + numPackets + "  ");
                System.out.println(String.format("%.2f", (((float) p.getSequenceNum() / numPackets) * 100)) + "% done.");
                System.out.println("cwnd = " + cwnd);
                PacketTimerCheckerThread ptct = new PacketTimerCheckerThread(this, p);
                ptct.start();
                timerThreads.add(ptct);
                // Sleep Thread
                Thread.sleep(1);
            }
            // Sleep thread
            Thread.sleep(70);
            // Make sure that we got acknowledgements
            for (int j = i; j <= Math.min(i + cwnd, numPackets); j++) {
                Packet p = packets[j - 1];
                while(!this.responseTable.get(p.getSequenceNum()).equalsIgnoreCase(PacketRecieverThread.ACK)) {
                    new PacketSenderThread(this, senderSocket, false, p, IP, PORT).start();
                    PacketTimerCheckerThread ptct = new PacketTimerCheckerThread(this, p);
                    ptct.start();
                    Thread.sleep(10);
                    failure = true;
                }
            }
            // Increment cwnd based on result
            i = i + cwnd;
            if(cwnd < ssThresh && !failure) {
                // Slow start
                cwnd *= 2;
            } else if(!failure) {
                // Congestion avoidance
                cwnd += 1;
            } else {
                // Something went wrong
                System.out.println("Failure detected, going back to slow start");
                System.out.println("Starting Congestion Control");
                ssThresh = Math.floorDiv(cwnd, 2);
                cwnd = 1;
            }
            failure = false;
        }
    }
}


